/*　　←複数行コメントアウトしてます。
課題10
内容

今回はMAを表示させます！
写経をして動作の確認をお願いします！
※写経ためAnswerファイルは存在しません。

・概要
iMA()関数を使ってMAを表示させます！

課題10おわり


★キーワード
・iMA()関数

※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"
#property link      "https://twitter.com/FXidure"
#property version   "1.00"
#property strict
#property indicator_chart_window

//まずはカスタムインジケータのプロパティ設定をします。
//この設定をしないとカスタムインジケータを表示させる事が出来ません。
// インジケータプロパティ設定
#property  indicator_buffers    1               // カスタムインジケータのバッファ数
#property  indicator_color1     clrWhite      // インジケータ1の色
#property  indicator_width1     2               // インジケータ1の太さ

//次に、カスタムインジケータ表示用の動的配列を宣言します。
// インジケータ表示用動的配列
double     IndBuffer[];        // インジケータ1表示用動的配列

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
// SetIndexBuffer( 0番目, 配列名 );
 SetIndexBuffer(0, IndBuffer);       // インジケータ1表示用動的配列をインジケータ1にバインドする

 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 /*
 先頭のバーだけ常時更新するようにします。
 先頭以外は一度描画してしまえば、それ以降処理は不要！
 */

 int total_Bars = rates_total - prev_calculated - 1;  // バー数取得(未計算分);  バーの総数　-　計算済みバーの数 - 要素番号として数えるため１引く

 if(total_Bars < 0){//total_Barsが負の数になるとエラーを起こすので回避します。
  total_Bars = 0;
 }

 for(int i = total_Bars ; i >= 0 ; i--){
  double get_MA = iMA(Symbol(), Period(), 25, 0, MODE_SMA, PRICE_CLOSE, i);
  IndBuffer[i] = get_MA;
 }


 /*上記を2行で書くことも出来ます！ for文が１行で処理できるときは{}波括弧を省略出来ます。

  for(int i=total_Bars ; i>=0 ; i--)
   IndBuffer[i] = iMA(Symbol(),Period(),25,0,MODE_SMA,PRICE_CLOSE,i);

 */

 return(rates_total);
}
//+------------------------------------------------------------------+


//iMA関数の説明
/*
 double get_ma = iMA (               // 移動平均算出
                      Symbol(),      // 通貨ペア
                      Period(),      // 時間軸
                      25,            // MAの平均期間
                      0,             // MAシフト
                      MODE_SMA,      // MAの平均化メソッド
                      PRICE_CLOSE,   // 適用価格
                      i              // シフト
                      );
*/



//+------------------------------------------------------------------+
