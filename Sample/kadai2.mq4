/*　　←複数行コメントアウトしてます。
課題2
内容

関数を自作してみましょう！
・値渡しについて

-----インジとしての想定される利用シーン-----
一連の処理を何度も行いたい時
スマートなソースコードを書くためには絶対必須

・概要
整数を”値渡し”で渡す。
渡した値を１０倍にする。
return()で値を返す。
帰ってきた値をPrint()で１０倍になっていることを確認する。

課題2おわり

★キーワード
・関数
・値渡し
・戻り値（返り値）
・int型の関数


※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           

#property strict                       
#property indicator_chart_window       



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 int a=10;
//以下に記述

 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+

//+関数部分------------------------------------------------------------------
int calc(int b){
int res=0;
//以下に記述

return(res);
}





