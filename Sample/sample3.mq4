/*　　←複数行コメントアウトしてます。
サンプル3

＊型の種類について
＊配列について

★キーワード
・配列（Array、アレイ）
-----------型の種類-----------------
int型           (整数しか扱えない)
double型        (小数点を扱う時)
bool型          (真、偽しか扱えない)
string型        (文字列を扱う時)

datetime型      (日付を扱う時)
color型         (色を扱う時)

他にもいくつかありますが、これだけわかればほぼすべてを表現出来ます。
その他の型
char型  

float型
long型
short型

void型

enum型(列挙型)　　　　int型をひとまとまりにする型
struct型(構造体型)  複数種類の型をひとまとまりにする型
----------------------------



※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           

#property strict                       
#property indicator_chart_window       

//型の種類について
//せっかくなので外部パラメータではどのように表示されるのかを確認してみます。
extern int sei=100;   //int型、整数
extern double doub=3.25;   //double型、小数点数
extern bool boooo=true;   //bool型、真偽値(true,falseの２種類)
extern string str="外部パラメータ";   //string型、文字列　（ダブルクォーテーションで囲うことで「文字列です」という意味になる！）
extern datetime DTime= D'2020.1.1 17:32:57';   //datetime型、日付
extern color clr=clrWhite;   //color型、カラー(R,G,B)

char cha = 0;
datetime t=100;
color c=clr;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
/*
配列について
配列は同じ型であれば複数格納できる。
MAやRSIなどの線を描画するインジケータは配列が使われています。
*/
 int arr[3];//←これが配列のて定義の仕方(要素数は３となります)
 arr[0]=0; //要素番号　0番目から始まる（ここを忘れやすいです）
 arr[1]=10;
 arr[2]=200;

 Print(arr[1]);

 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+
