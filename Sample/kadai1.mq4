/*　　←複数行コメントアウトしてます。
課題１
内容

外部パラメータに入力した数値の回数分のfor文をループさせるプログラムを作りましょう！
一度だけ処理したいのでコードはOnInitの中で書きましょう！

-----インジとしての想定される利用シーン-----
指定したバーの本数分しか計算を行わない処理をする時

・概要
[入力]　外部パラメータ
[出力]　Print()でエキスパートに出力

・Hint
>
<
>=
<=
!=
==
いずれかを使用します！

課題１おわり


※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           

#property strict                       
#property indicator_chart_window       
//kadai1
//ここで外部パラメータを宣言しましょう！


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 
 for(int i=0 ;  ;i++){//括弧の中の条件部分を記述してください。
  Print(i);
 }
 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+
