/*　　←複数行コメントアウトしてます。
課題8
内容

今回から実践的な内容になります！
そういうものなんだ！で結構なので頑張ってついていきましょう！
写経をして自身で作成したものが正常に動作するか確認しましょう！

・概要
終値を線で結んで描画する！（ティック足の表現）


課題8おわり

★キーワード
・動的配列
・SetIndex(バインド、紐付け)

※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       

//まずはカスタムインジケータのプロパティ設定をします。
//この設定をしないとカスタムインジケータを表示させる事が出来ません。
// インジケータプロパティ設定
#property  indicator_buffers    1               // カスタムインジケータのバッファ数
#property  indicator_color1     clrWhite      // インジケータ1の色
#property  indicator_width1     2               // インジケータ1の太さ

//次に、カスタムインジケータ表示用の動的配列を宣言します。
// インジケータ表示用動的配列
double     IndBuffer[];        // インジケータ1表示用動的配列

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
//そして先程プロパティ設定で用意したインジケータバッファに、変数配列をバインド(紐付け)します。
//これはプログラム実行中に1度だけ行う必要があるので、OnInitイベント関数内で行います。
// SetIndexBuffer( 0番目, 配列名 );
 SetIndexBuffer( 0, IndBuffer );     // インジケータ1表示用動的配列をインジケータ1にバインドする

 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 IndBuffer[0] = Close[0];
 IndBuffer[1] = Close[1];
 IndBuffer[2] = Close[2];
 IndBuffer[3] = Close[3];
 IndBuffer[4] = Close[4];
 
 //直近５本の終値をバッファー配列に設定したので、５本文の終値ラインが表示されます。
 //次回はループを使いバーの本数分表示させていきます！
 
 return(rates_total);
}
//+------------------------------------------------------------------+




