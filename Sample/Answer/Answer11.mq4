/*　　←複数行コメントアウトしてます。
課題11
内容

今回はMAを２本表示させます！
デフォルトのソースはkadai10で写経したコードと同じ内容です。
これはご自身の力で頑張りましょう!


・概要
iMA()関数を使って２種類の期間のMAを2本表示させます！

次回のアロー表示に使います！


課題11おわり


★キーワード
・iMA()関数

※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       

//まずはカスタムインジケータのプロパティ設定をします。
//この設定をしないとカスタムインジケータを表示させる事が出来ません。
// インジケータプロパティ設定
#property  indicator_buffers    2               // カスタムインジケータのバッファ数
#property  indicator_color1     clrWhite      // インジケータ1の色
#property  indicator_width1     2               // インジケータ1の太さ
#property  indicator_color2     clrLime      // インジケータ1の色
#property  indicator_width2     2               // インジケータ1の太さ

//次に、カスタムインジケータ表示用の動的配列を宣言します。
// インジケータ表示用動的配列
double     IndBuffer[];        // インジケータ1表示用動的配列
double     IndBuffer2[];        // インジケータ1表示用動的配列

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
// SetIndexBuffer( 0番目, 配列名 );
 SetIndexBuffer( 0, IndBuffer );     // インジケータ1表示用動的配列をインジケータ1にバインドする
 SetIndexBuffer( 1, IndBuffer2 );     // インジケータ1表示用動的配列をインジケータ1にバインドする

 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 int total_Bars = rates_total - prev_calculated - 1;  // バー数取得(未計算分);  バーの総数　-　計算済みバーの数 - 要素番号として数えるため１引く

 if(total_Bars < 0){//total_Barsが負の数になるとエラーを起こすので回避します。
  total_Bars = 0;
 }

 for(int i=total_Bars ; i>=0 ; i--){
  IndBuffer[i] = iMA(Symbol(),Period(),25,0,MODE_SMA,PRICE_CLOSE,i);
  IndBuffer2[i] = iMA(Symbol(),Period(),75,0,MODE_SMA,PRICE_CLOSE,i);
 }

 return(rates_total);
}
//+------------------------------------------------------------------+


