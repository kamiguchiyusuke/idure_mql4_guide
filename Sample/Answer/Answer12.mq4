/*　　←複数行コメントアウトしてます。
課題12
内容

今回はアローを表示させていきましょう！
if文の中を書き換える課題になってます。
内容が少し難しく感じるかもしれませんが、ほとんど同じことの繰り返しをしているので気負わずに頑張りましょう。

・概要
ゴールデンクロス、デッドクロスでのアローの表示。
ゴールデンクロス、デッドクロスの判定条件について
＾ゴールデンクロスの例
一つ前のバー　　短期線＜長期線 (短期線の上に長期線がある)
最新のバー　　　短期線＞長期線 (長期線の上に短期戦が来る)
という状態になった時に初めてそのタイミングでクロスした事が判定できる。

課題12おわり


★キーワード
・SetIndexArrow()
・if文の書き方は調べてみましょう！
&& かつ
||　または
>
<
>=
<=
!=
==



※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       

// インジケータプロパティ設定
#property  indicator_buffers    4               // カスタムインジケータのバッファ数
//MA用のプロパティ設定
#property  indicator_color1     clrWhite      // インジケータ1の色
#property  indicator_width1     2               // インジケータ1の太さ
#property  indicator_color2     clrLime      // インジケータ2の色
#property  indicator_width2     2               // インジケータ2の太さ

//アロー用プロパティ設定
#property  indicator_color3     clrRed      // インジケータ3の色
#property  indicator_width3     2               // インジケータ3の太さ
#property  indicator_color4     clrBlue      // インジケータ4の色
#property  indicator_width4     2               // インジケータ4の太さ

//次に、カスタムインジケータ表示用の動的配列を宣言します。
// インジケータ表示用動的配列
double     IndBuffer1[];        // インジケータ1表示用動的配列
double     IndBuffer2[];        // インジケータ2表示用動的配列

//アロー表示用動的配列
double     ArrowUP_Buffer[];
double     ArrowDW_Buffer[];


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
// SetIndexBuffer( 0番目, 配列名 );
 SetIndexBuffer( 0, IndBuffer1 );     // 紐付け
 SetIndexBuffer( 1, IndBuffer2 );     // 紐付け
 SetIndexBuffer( 2, ArrowUP_Buffer );     // 紐付け
 SetIndexBuffer( 3, ArrowDW_Buffer );     // 紐付け


//バッファーをアローとして使うときは「このバッファーはアローですよー」と設定しなければいけません
 SetIndexStyle(2 ,DRAW_ARROW);           // インジケータスタイルをアローとして設定
 SetIndexArrow(2,241);//紐付けした２番はアロー設定(アローコード)

 SetIndexStyle(3 ,DRAW_ARROW);           // インジケータスタイルをアローとして設定
 SetIndexArrow(3,242);//紐付けした３番はアロー設定(アローコード)


 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{

 int total_Bars = rates_total - prev_calculated - 2;  //2を引く（超重要） 1にすると125行目でエラーになります。詳細は140行目に記載

 if(total_Bars < 0){//total_Barsが負の数になるとエラーを起こすので回避します。
  total_Bars = 0;
 }

 for(int i=total_Bars ; i>=0 ; i--){
  IndBuffer1[i] = iMA(Symbol(),Period(),25,0,MODE_SMA,PRICE_CLOSE,i);
  IndBuffer2[i] = iMA(Symbol(),Period(),50,0,MODE_SMA,PRICE_CLOSE,i);

  //1本前が短期線が下、中期線が上、現行足が短期線が上、中期線が下。　になった瞬間アローを表示する。
  if(IndBuffer1[i+1]<IndBuffer2[i+1] && IndBuffer1[i]>IndBuffer2[i]){//ゴールデンクロス
   ArrowUP_Buffer[i] = Low[i];
  }
  //1本前が短期線が上、中期線が下、現行足が短期線が下、中期線が上。　になった瞬間アローを表示する。
  else if(IndBuffer1[i+1]>IndBuffer2[i+1] && IndBuffer1[i]<IndBuffer2[i]){//デッドクロス
   ArrowDW_Buffer[i] = High[i];
  }

 }

 return(rates_total);
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//|rates_total - prev_calculated - 2
//|
//| 今回の課題では125,129行目で配列の中が[i+1]となっている
//| バーの本数が10000本だとして上記の式を計算すると
//| 9998 = 10000 - 0 - 2　　　となります
//| 一番最初の i は 9998 で i+1 は9999 になります
//| つまり配列の数が10000あった場合、一番最後の番号は[9999]になるので、
//| 最初に 2 を引く必要があります。
//+------------------------------------------------------------------+
