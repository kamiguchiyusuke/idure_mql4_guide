/*　　←複数行コメントアウトしてます。
課題5
内容

for文とif文の練習！
かなり重要になってきますがその分挫折しやすい部分でもあります。
あまり難しく考えずに取り組んでいきましょう！

・基本問題
1から30までカウントアップしていき、
偶数だけをPrint（）で出力しましょう！

応用問題
30から1までカウントダウンしていき、
奇数だけをPrint（）で出力しましょう！



課題5おわり

★キーワード
・for文
・if文

※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 int loop=0;

//偶数
 for(loop=1 ;loop<=30 ;loop++ ){
  if(loop%2==0){
   Print(loop);
  }
 }

//奇数(波括弧無し)
 for(loop=30 ;loop>0 ;loop-- )
  if(loop%2==1)
   Print(loop);

  
 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{









 return(rates_total);
}
//+------------------------------------------------------------------+




