/*　　←複数行コメントアウトしてます。
課題4
内容


既存の関数を使ってみよう！
今まで使ってきたPrint()も関数です。
標準で沢山の関数が用意されてます。
したがって関数を使用するために大切な事は「調べる」ことになります！

-----インジとしての想定される利用シーン-----
様々なインジで利用します！


・概要
その日の曜日を取得する関数を使う
取得した数値をif文で何曜日なのか判定して「○曜日」のように文字列を出力する


・応用問題
1.関数を利用していかにスマートにできるか考える。
2.switch文について調べてif文を使わずに実装してみる。

課題４おわり

★キーワード
・mql 曜日取得
・switch文


※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           

#property strict                       
#property indicator_chart_window       



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 int day=DayOfWeek();//0(日)~6(土)


//if文のパターン
 if(day==0){
  Print("日曜日");
 }
 else if(day==1)
  Print("月曜日");
 else if(day==2)
  Print("火曜日");
 else if(day==3)
  Print("水曜日");
 else if(day==4)
  Print("木曜日");
 else if(day==5)
  Print("金曜日");
 else if(day==6)
  Print("土曜日");
 else 
 Print("error");


//switch文のパターン
 switch(day){
  case 0:
  Print("日曜");
  break;
  case 1:
  Print("月曜");
  break;
  case 2:
  Print("火曜");
  break;
  case 3:
  Print("水曜");
  break;
  case 4:
  Print("木曜");
  break;
  case 5:
  Print("金曜");
  break;
  case 6:
  Print("土曜");
  break;
 }


//配列を使ったパターン
 string day_arr[7]={"日曜日","月曜日","火曜日","水曜日","木曜日","金曜日","土曜日"};
 Print(day_arr[day]);





 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+


