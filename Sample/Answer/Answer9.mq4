/*　　←複数行コメントアウトしてます。
課題9
内容

前回の内容の延長になります！

・概要
終値を線で結んで描画する！（ティック足の表現）
前回の課題の続きに写経して動作を確認してください!

コンパイルできますが、このままでは表示されません！
なぜ表示されないのかを考えましょう！（沢山の人がハマるポイントになります。表示されない理由を知ることを目的としています。）

ヒント
MT4のターミナルに[array out of range]というエラーが出ている。
Array（配列）が範囲を超えました。という意味です。
どの行でエラーになってるか確認する。

大ヒント
ロウソクの本数1000本のとき配列の要素数は1000個
要素番号は0~999となる。

課題9おわり

★キーワード
・array out of range
・配列


※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       

//まずはカスタムインジケータのプロパティ設定をします。
//この設定をしないとカスタムインジケータを表示させる事が出来ません。
// インジケータプロパティ設定
#property  indicator_buffers    1               // カスタムインジケータのバッファ数
#property  indicator_color1     clrWhite      // インジケータ1の色
#property  indicator_width1     2               // インジケータ1の太さ

//次に、カスタムインジケータ表示用の動的配列を宣言します。
// インジケータ表示用動的配列
double     IndBuffer[];        // インジケータ1表示用動的配列

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
// SetIndexBuffer( 0番目, 配列名 );
 SetIndexBuffer( 0, IndBuffer );     // インジケータ1表示用動的配列をインジケータ1にバインドする

 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
/*
先頭のバーだけ常時更新するようにします。
先頭以外は一度描画してしまえば、それ以降処理は不要！
*/
 int total_Bars = rates_total - prev_calculated;  // バーの総数　-　計算済みバーの数 

 if( total_Bars == Bars) {
  total_Bars = Bars-1;   //ヒント! ここの行を変更します。
 } 
 
 for(int i=total_Bars ; i>=0 ; i--){
  IndBuffer[i] = Close[i];
 }

 return(rates_total);
}
//+------------------------------------------------------------------+




