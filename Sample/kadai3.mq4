/*　　←複数行コメントアウトしてます。
課題3
内容

関数を自作してみましょう！２
・参照渡しについて
値渡しと参照渡しの違いは理解すべきことなので、覚えましょう！

-----インジとしての想定される利用シーン-----
一連の処理を何度も行いたい時
スマートなソースコードを書くためには絶対必須

・概要
void型の関数を作る。
整数を”参照渡し”で渡す
渡した値を２０倍にする


課題３おわり

★キーワード
・void型関数
・参照渡し(アドレス渡し)

※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           

#property strict                       
#property indicator_chart_window       



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 int a=10;

            //ここで関数呼び出し

 Print(a);


 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+

//以下に関数を記述してください！
void calc(){
}
