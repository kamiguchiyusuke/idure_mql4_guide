/*　　←複数行コメントアウトしてます。
課題6
内容

for文とif文の練習！2
for文を途中で抜ける方法！

・基本問題
1から３０までカウントアップしていく！
20までカウントしたらfor文を抜ける。


・応用問題
for文をスキップする方法！
1から３０までカウントアップしていく！
20だけスキップする！
注意点、Print（）が処理される位置に注意

課題6おわり

★キーワード
・break;     (ブレイク文)
・continue;  (コンティニュー文)


※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 int loop;

 for(loop=1;loop<=30;loop++){
  Print(loop);
  if(0){   //記述部分--------------if文の中を書き換えてください
   //記述部分
  }
 }
  
 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+




