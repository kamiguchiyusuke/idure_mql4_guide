/*　　←複数行コメントアウトしてます。
課題7
内容

for文とif文の練習！2
２重ループをマスターしよう！
ここでは記述した内容を理解するだけで大丈夫です！
実際に実行してみてください！
※Answerファイルは有りません。

・基本問題
１から９までの九九を作成します！

 1 2 3 4 5 6 7 8 9
 2 4 6 . . . .
 3
 4
 5
 6
 7
 8
 9

イメージはこんな表になります！



課題7おわり
次回から実践的なコードになります！

★キーワード
・２重ループ
・型変換（キャストという）

※わからないことはオープンチャットの初心者の掲示板まで！！！

*///←ここまで複数行コメントアウト



//+------------------------------------------------------------------+
//|                                                      sample1.mq4 |
//|                                                            idure |
//|                                      https://twitter.com/FXidure |
//+------------------------------------------------------------------+
#property copyright "idure"                          
#property link      "https://twitter.com/FXidure"    
#property version   "1.00"                           
#property strict                       
#property indicator_chart_window       



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
{
 int i,j;
 string str;

 for(i=1;i<=9;i++){
  for(j=1;j<=9;j++){
   str += (string)(i*j);  //型変換（キャスト）してます。 i×jを計算して文字に変換
   str += " ";  //          str += ←この書き方は空白文字を後尾に連結させてます！
  }
  Print(str);//1行分を表示
  str="";//空っぽにする
 }
  
 return(INIT_SUCCEEDED);
}


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
 return(rates_total);
}
//+------------------------------------------------------------------+




